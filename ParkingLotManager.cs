﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ParkingLot
{
    public class ParkingLotManager
    {
        private static Mutex mMutex;
        private Dictionary<string, int> mParkingLotCounts;
        private Dictionary<string, int> mParkingLotMaxCounts;

        public ParkingLotManager()
        {
            mMutex = new Mutex();
            mParkingLotCounts = new Dictionary<string, int>();
            mParkingLotMaxCounts = new Dictionary<string, int>();

            //add the initial data to the tables (this should be done in the database)
            mParkingLotCounts["Austin"] = 0;
            mParkingLotMaxCounts["Austin"] = 123456;
            mParkingLotCounts["Dallas"] = 0;
            mParkingLotMaxCounts["Dallas"] = 123456789;
            mParkingLotCounts["San Antonio"] = 0;
            mParkingLotMaxCounts["San Antonio"] = 1234;
            mParkingLotCounts["Houston"] = 0;
            mParkingLotMaxCounts["Houston"] = 1000000000;
        }

        ~ParkingLotManager()
        {
            mMutex.Dispose();
        }

        public int UpdateParkingLotCount(string parkingLotLocation, int countAdjustment)
        {
            mMutex.WaitOne();
            int returnCount = -1;

            if(mParkingLotCounts.ContainsKey(parkingLotLocation))
            {
                int newCount = mParkingLotCounts[parkingLotLocation] + countAdjustment;

                if(newCount >= 0  && newCount <= mParkingLotMaxCounts[parkingLotLocation])
                {
                    mParkingLotCounts[parkingLotLocation] += countAdjustment;
                    returnCount = mParkingLotCounts[parkingLotLocation];
                }
            }

            mMutex.ReleaseMutex();
            return returnCount;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ParkingLot.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ParkingLotCountController : ControllerBase
    {
        private static ParkingLotManager mParkingLotManager;

        public ParkingLotCountController()
        {
            if(mParkingLotManager == null)
            {
                mParkingLotManager = new ParkingLotManager();
            }
        }

        [HttpPost]
        public int Post([FromBody] string parkingLotLocation, [FromBody] int countAdjustment)
        {
            return mParkingLotManager.UpdateParkingLotCount(parkingLotLocation, countAdjustment);
        }
    }
}
